
# 参考：
# https://support.huaweicloud.com/sdk-python-devg-obs/obs_22_0906.html
from obs import ObsClient

# 创建ObsClient实例
obsClient = ObsClient(

)

def callback(transferredAmount, totalAmount, totalSeconds):
    # 获取下载平均速率(KB/S)
    print(transferredAmount * 1.0 / totalSeconds / 1024)
    # 获取下载进度百分比
    print(transferredAmount * 100.0 / totalAmount)


# 对象名，即上传后的文件名。
fileName = ''

# 待上传的本地文件，如aa/bb.txt
uploadFilePath= ''

taskNum = 6
partSize = 1000 * 1024 * 1024
enableCheckpoint = True
try:
    resp = obsClient.uploadFile('mabukuai-backend-go', fileName, uploadFilePath, partSize, taskNum, enableCheckpoint, progressCallback=callback)
    if resp.status < 300:    
        print('requestId:', resp.requestId)    
    else:    
        print('errorCode:', resp.errorCode)    
        print('errorMessage:', resp.errorMessage)
except:
    import traceback
    print(traceback.format_exc())

    # 关闭obsClient
    obsClient.close()
